cd $PSScriptRoot
$installerUrl = "http://192.168.81.181:800/SeleniumRunner/setup-SeleniumRunner.exe"
$installerExe = "setup-SeleniumRunner.exe"
(New-Object System.Net.WebClient).DownloadFile($installerUrl, $installerExe)
Start-Process -NoNewWindow -Wait $installerExe -ArgumentList "/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /LOG=Bin\seleniumRunnerInstallation.log /CLOSEAPPLICATIONS /FORCECLOSEAPPLICATIONS /DIR=Bin"
Remove-Item -Force $installerExe     
             